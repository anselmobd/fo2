from django.urls import re_path

import logistica.views
import logistica.views.ajax
import logistica.views.entrada_nf


app_name = 'logistica'
urlpatterns = [
    re_path(r"^$", logistica.views.index.index, name="index"),
    re_path(
        r"^notafiscal_rel/$",
        logistica.views.nf.consulta.NotafiscalRel.as_view(),
        name="notafiscal_rel",
    ),
    re_path(
        r"^notafiscal_rel/(?P<dia>\d+)/(?P<mes>\d+)/(?P<ano>\d+)/$",
        logistica.views.nf.consulta.NotafiscalRel.as_view(),
        name="notafiscal_rel__get",
    ),
    re_path(
        r"^notafiscal_chave/(?P<chave>\d+)?/?$",
        logistica.views.notafiscal_chave.NotafiscalChave.as_view(),
        name="notafiscal_chave",
    ),
    re_path(
        r"^notafiscal_nf/(?P<nf>\d+)?/?$",
        logistica.views.nf.numero_nf.notafiscal_nf,
        name="notafiscal_nf",
    ),
    re_path(
        r"^notafiscal_embarcando/$",
        logistica.views.nf.embarcando.NotafiscalEmbarcando.as_view(),
        name="notafiscal_embarcando",
    ),
    re_path(
        r"^notafiscal_movimentadas/$",
        logistica.views.nf.movimentada.NotafiscalMovimentadas.as_view(),
        name="notafiscal_movimentadas",
    ),
    re_path(
        r"^entrada_nf/lista/$",
        logistica.views.entrada_nf.EntradaNfLista.as_view(),
        name="entr_nf_lista",
    ),
    re_path(
        r"^entrada_nf/historico/(?P<id>[^/]+)/$",
        logistica.views.entr_nf_historico.EntradaNfHistorico.as_view(),
        name="entr_nf_historico",
    ),
    re_path(
        r"^ajax/entr_nf_cadastro/(?P<cadastro>[^/]+)/$",
        logistica.views.ajax.entr_nf_cadastro,
        name="ajax_entr_nf_cadastro",
    ),
]
